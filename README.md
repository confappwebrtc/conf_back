# Setup the application 
To start the application run the following code in the terminal:
```
$ docker-compose build --no-cache  
$ docker-compose up
```
Check the API documentation on http://localhost:9001/doc/
*Docker should be installed on the device
