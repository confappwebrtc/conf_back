const express = require('express')
var cors = require('cors')
const bodyParser = require('body-parser')
const app = express()
const dotenv = require('dotenv')
const Sequelize = require('sequelize');
var initModels = require("./src/models/init-models");

const swaggerUi = require('swagger-ui-express')
const swaggerFile = require('./swagger-output.json')

app.use(cors({
  origin: ["http://localhost:9001", "http://0.0.0.0:9001", "http://localhost:32871"],
  credentials: true,
}));

dotenv.config();

const port = process.env.PORT;

const path = `postgres://${process.env.PG_USER}:${process.env.PG_PASSWORD}@${process.env.PG_HOST}:${process.env.PG_PORT}/${process.env.PG_DATABASE}`;
const sequelize = new Sequelize(path, {
  logging: console.log()
});

var models = initModels(sequelize);

sequelize.sync().catch((e) => {console.log(e)}); // sync({force: true}) to force create tables


app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)

app.get('/', (request, response) => {
    response.json({ info: 'Node.js, Express, and Postgres API' })
  })


app.use('/doc', swaggerUi.serve, swaggerUi.setup(swaggerFile))
app.listen(port, () => {
console.log(`App running on port ${port}.`)
})


/* Endpoints */
require('./src/routes')(app)