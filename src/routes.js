const verifyToken = require("./middleware/auth");
const verifyRole = require("./middleware/authRole");
const { decodeProto } = require("./middleware/proto");
const db = require("./services/queries");


module.exports = function (app) {

    app.post('/user/login', decodeProto("User"), db.loginUser) /* data: User (only Login and Password) */

    app.post('/user/guest/register', decodeProto("User"), db.registerUser) /* data: User (Login and Password) */

    app.post('/conference/one', decodeProto("Conference"), db.getConferenceById) /* data: Conference (Only Id) */

    app.get('/conference/all', db.getConferences) /* Empty Body */

    app.use(verifyToken);

    app.post('/user/role/register', verifyRole(["Admin"]), decodeProto("User"), db.registerUserWithRole)          /* data: User (Login + Password + Role) */

    app.post('/user/speaker/register', verifyRole(["Admin","Moderator"]), decodeProto("User"), db.registerUserMod)     /* data: User (With Promo || Speaker || Guest) */

    app.post('/user/promo/register', verifyRole(["Admin","Moderator"]), decodeProto("User"), db.registerUserMod)        /* data: User (With Promo || Speaker || Guest) */

    app.get('/user', (req, res, next) => {req.body.obj=req.user; next();}, db.getUser);  /* Empty Body */

    app.post('/user/info', verifyRole(["Admin"]),  decodeProto("User"), db.getUser);  /* data: User  (only id) */

    app.post('/user/update',  decodeProto("User"), db.updateUser) /* data: User  (only Login and Password) */

    app.put('/user/info/self-update', (req, res, next) => {req.body.obj=req.user; next();}, db.updateUserInfo) /* data: User (With Promo || Speaker || Guest)*/

    app.put('/user/info/admin-update', verifyRole(["Admin"]), decodeProto("User"), db.updateUserInfo) /* data: User (With Promo || Speaker || Guest)*/
    
    app.get('/roles', verifyRole(["Admin"]), db.getRoles) /* Empty Body */

    app.get('/permessions', verifyRole(["Admin","Moderator"]), db.getPermissions) /* Empty Body */

    app.post('/conference/new', verifyRole(["Admin","Moderator"]), decodeProto("Conference"), db.createConference) /* data: Conference */

    app.put('/conference/update', verifyRole(["Admin","Moderator"]), decodeProto("Conference"), db.updateConference) /* data: Conference */

    app.post('/conference/sections', decodeProto("Section"), db.getSections) /* data: Conference (Only Id) */

    app.post('/conference/section/new', verifyRole(["Admin","Moderator"]), decodeProto("Section"), db.addSection) /* data: Section */

    app.put('/conference/section/update', verifyRole(["Admin","Moderator"]), decodeProto("Section"), db.updateSection) /* data: Section */

    app.post('/conference/section/panel/new', verifyRole(["Admin","Moderator"]), decodeProto("Panel"), db.addPanel) /* data: Panel */

    app.post('/conference/section/panel/all', decodeProto("Section"), db.getPanelsBySection) /* data: Section (Only Id) */

    app.post('/conference/section/panel/one', decodeProto("Panel"), db.getPanelById); /* data: Panel  (Only Id) */

    app.put('/conference/section/panel/update', verifyRole(["Admin","Moderator"]), decodeProto("Panel"), db.updatePanel) /* data: Panel */

    app.post('/conference/section/panel/polls', decodeProto("Panel"), db.getPollsByPanel) /* data: Panel */


}