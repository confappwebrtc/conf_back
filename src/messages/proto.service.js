const protobuf = require('protobufjs');
const userTypes = ['Speaker', 'Promo', 'Guest', 'Permission', 'Permissions', 'User', 'User_row'];

//const protoFile = 'user.proto';

async function encode (data, messageType, repeatedType) {
        const protoFile = (userTypes.includes(messageType))? "/app/src/messages/user.proto": "/app/src/messages/conference.proto";

        const root = await protobuf.load(protoFile);
        const protoType = root.lookupType(`conferencepackage.${messageType}`);

        var dataJSON = {};
        if(repeatedType != null) dataJSON[repeatedType] = data;
        else dataJSON = data;
        try{
        buf = protoType.encode(dataJSON).finish();
        
        return buf.toString('base64');
        }catch(e){
            return false;
        }
}

async function decode (data, messageType) {

    const protoFile = (userTypes.includes(messageType))?  'src/messages/user.proto': 'src/messages/conference.proto';

    const root = await protobuf.load(protoFile);
    const protoType = root.lookupType(`conferencepackage.${messageType}`);
    
    try{
        const obj = protoType.decode(Buffer.from(data, 'base64'))
        return obj;
    }catch(e){
        return false;
    }
    
    
}

module.exports = {
    encode,
    decode
}