const protobuf = require('../messages/proto.service');

function decodeProto(messageType) {

    return async function(req, res, next) {
        if(req.body.obj) next();
        else{
        const obj = await protobuf.decode(req.body.data, messageType);
        if(!obj) {
            const buf = await protobuf.encode({error: "Wrong Object Type"}, "Error");
            return res.status(409).send(buf);
        }
        req.body.obj = obj;
        next();
        }
    }
}

module.exports = {
    decodeProto
}