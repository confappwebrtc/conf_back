const jwt = require("jsonwebtoken");
const protobuf = require('../messages/proto.service');


const verifyToken = async (req, res, next) => {
  const token =
    req.body.token || req.headers["x-access-token"];

  if (!token) {
    const buf = await protobuf.encode({error: "Please Sign Up or Login"}, "Error");
    return res.status(409).send(buf);
  }
  try {
    const decoded = jwt.verify(token, process.env.TOKEN_KEY);
    req.user = decoded;
  } catch (err) {
    const buf = await protobuf.encode({error: "Please Sign Up or Login"}, "Error");
    return res.status(409).send(buf);
  }
  return next();
};

module.exports = verifyToken;