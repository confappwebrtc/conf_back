const db = require('../services/db.service');
const pool = db.getPool();
const protobuf = require('../messages/proto.service');

function verifyRole(roleNames) {
    return async function(req, res, next) {
        if(!req.user || !req.user.role_id){
            pool.query(`SELECT * FROM public.roles where id='${req.user.role_id}';`, async (error, results) => {
                if (error || results.rows.length === 0) {
                  const buf = await protobuf.encode({ error: "Not Allowed" }, "Error"); //Rebuilt
                  res.status(401).send(buf);
                }
                else {
                    if(roleNames.include(results.rows[0].name)) next();
                    else {
                        const buf = await protobuf.encode({ error: "Not Allowed" }, "Error"); //Rebuilt
                        res.status(401).send(buf);
                    }
                }
              })
        }
    }
}

module.exports = verifyRole;
