const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('contact_info', {
    id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    speaker_id: {
      type: DataTypes.STRING(36),
      allowNull: true,
      references: {
        model: 'speaker',
        key: 'id'
      }
    },
    promo_id: {
      type: DataTypes.STRING(36),
      allowNull: true,
      references: {
        model: 'promo',
        key: 'id'
      }
    },
    email: {
      type: DataTypes.STRING,
      allowNull: true
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: true
    },
    telegram: {
      type: DataTypes.STRING,
      allowNull: true
    },
    instagram: {
      type: DataTypes.STRING,
      allowNull: true
    },
    vk: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'contact_info',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "contact_info_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
