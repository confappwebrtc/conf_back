var DataTypes = require("sequelize").DataTypes;
var _chat = require("./chat");
var _conference = require("./conference");
var _contact_conference = require("./contact_conference");
var _contact_info = require("./contact_info");
var _extra_permissions = require("./extra_permissions");
var _guest = require("./guest");
var _panel = require("./panel");
var _permissions = require("./permissions");
var _poll = require("./poll");
var _poll_answers = require("./poll_answers");
var _poll_user_answers = require("./poll_user_answers");
var _promo = require("./promo");
var _role_permission = require("./role_permission");
var _roles = require("./roles");
var _sections = require("./sections");
var _speaker = require("./speaker");
var _users = require("./users");

function initModels(sequelize) {
  
  var speaker = _speaker(sequelize, DataTypes);
  var promo = _promo(sequelize, DataTypes);
  var guest = _guest(sequelize, DataTypes);
  var roles = _roles(sequelize, DataTypes);
  var users = _users(sequelize, DataTypes);
  var conference = _conference(sequelize, DataTypes);
  var contact_conference = _contact_conference(sequelize, DataTypes);
  var sections = _sections(sequelize, DataTypes);
  var panel = _panel(sequelize, DataTypes);
  var chat = _chat(sequelize, DataTypes);
  var poll = _poll(sequelize, DataTypes);
  var poll_answers = _poll_answers(sequelize, DataTypes);
  var poll_user_answers = _poll_user_answers(sequelize, DataTypes);
  var permissions = _permissions(sequelize, DataTypes);
  var extra_permissions = _extra_permissions(sequelize, DataTypes);
  var role_permission = _role_permission(sequelize, DataTypes);
  var contact_info = _contact_info(sequelize, DataTypes);

  contact_conference.belongsTo(conference, { as: "conference", foreignKey: "conference_id"});
  conference.hasMany(contact_conference, { as: "contact_conferences", foreignKey: "conference_id"});
  sections.belongsTo(conference, { as: "conference", foreignKey: "conference_id"});
  conference.hasMany(sections, { as: "sections", foreignKey: "conference_id"});
  users.belongsTo(guest, { as: "guest", foreignKey: "guest_id"});
  guest.hasMany(users, { as: "users", foreignKey: "guest_id"});
  chat.belongsTo(panel, { as: "panel", foreignKey: "panel_id"});
  panel.hasMany(chat, { as: "chats", foreignKey: "panel_id"});
  poll.belongsTo(panel, { as: "panel", foreignKey: "panel_id"});
  panel.hasMany(poll, { as: "polls", foreignKey: "panel_id"});
  extra_permissions.belongsTo(permissions, { as: "permission", foreignKey: "permissions_id"});
  permissions.hasMany(extra_permissions, { as: "extra_permissions", foreignKey: "permissions_id"});
  role_permission.belongsTo(permissions, { as: "permission", foreignKey: "permissions_id"});
  permissions.hasMany(role_permission, { as: "role_permissions", foreignKey: "permissions_id"});
  poll_answers.belongsTo(poll, { as: "question", foreignKey: "question_id"});
  poll.hasMany(poll_answers, { as: "poll_answers", foreignKey: "question_id"});
  poll_user_answers.belongsTo(poll_answers, { as: "answer_number_poll_answer", foreignKey: "answer_number"});
  poll_answers.hasMany(poll_user_answers, { as: "poll_user_answers", foreignKey: "answer_number"});
  contact_info.belongsTo(promo, { as: "promo", foreignKey: "promo_id"});
  promo.hasMany(contact_info, { as: "contact_infos", foreignKey: "promo_id"});
  users.belongsTo(promo, { as: "promo", foreignKey: "promo_id"});
  promo.hasMany(users, { as: "users", foreignKey: "promo_id"});
  role_permission.belongsTo(roles, { as: "role", foreignKey: "role_id"});
  roles.hasMany(role_permission, { as: "role_permissions", foreignKey: "role_id"});
  users.belongsTo(roles, { as: "role", foreignKey: "role_id"});
  roles.hasMany(users, { as: "users", foreignKey: "role_id"});
  panel.belongsTo(sections, { as: "section", foreignKey: "section_id"});
  sections.hasMany(panel, { as: "panels", foreignKey: "section_id"});
  contact_info.belongsTo(speaker, { as: "speaker", foreignKey: "speaker_id"});
  speaker.hasMany(contact_info, { as: "contact_infos", foreignKey: "speaker_id"});
  users.belongsTo(speaker, { as: "speaker", foreignKey: "speaker_id"});
  speaker.hasMany(users, { as: "users", foreignKey: "speaker_id"});
  chat.belongsTo(users, { as: "user", foreignKey: "user_id"});
  users.hasMany(chat, { as: "chats", foreignKey: "user_id"});
  extra_permissions.belongsTo(users, { as: "user", foreignKey: "user_id"});
  users.hasMany(extra_permissions, { as: "extra_permissions", foreignKey: "user_id"});
  panel.belongsTo(users, { as: "user", foreignKey: "user_id"});
  users.hasMany(panel, { as: "panels", foreignKey: "user_id"});
  poll_user_answers.belongsTo(users, { as: "user", foreignKey: "user_id"});
  users.hasMany(poll_user_answers, { as: "poll_user_answers", foreignKey: "user_id"});

  return {
    chat,
    conference,
    contact_conference,
    contact_info,
    extra_permissions,
    guest,
    panel,
    permissions,
    poll,
    poll_answers,
    poll_user_answers,
    promo,
    role_permission,
    roles,
    sections,
    speaker,
    users,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
