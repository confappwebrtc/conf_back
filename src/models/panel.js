const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('panel', {
    id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    title: {
      type: DataTypes.STRING,
      allowNull: true
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true
    },
    starts: {
      type: DataTypes.DATE,
      allowNull: true
    },
    duration: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    section_id: {
      type: DataTypes.STRING(36),
      allowNull: true,
      references: {
        model: 'sections',
        key: 'id'
      }
    },
    user_id: {
      type: DataTypes.STRING(36),
      allowNull: true,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    raiting: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    status: {
      type: DataTypes.STRING(60),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'panel',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "panel_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
