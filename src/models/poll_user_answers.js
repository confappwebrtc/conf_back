const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('poll_user_answers', {
    id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    user_id: {
      type: DataTypes.STRING(36),
      allowNull: true,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    question_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    answer_number: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'poll_answers',
        key: 'number'
      }
    }
  }, {
    sequelize,
    tableName: 'poll_user_answers',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "poll_user_answers_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
