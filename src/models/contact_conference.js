const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('contact_conference', {
    id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    conference_id: {
      type: DataTypes.STRING(36),
      allowNull: true,
      references: {
        model: 'conference',
        key: 'id'
      }
    },
    email: {
      type: DataTypes.STRING,
      allowNull: true
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: true
    },
    telegram: {
      type: DataTypes.STRING,
      allowNull: true
    },
    instagram: {
      type: DataTypes.STRING,
      allowNull: true
    },
    vk: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'contact_conference',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "contact_conference_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
