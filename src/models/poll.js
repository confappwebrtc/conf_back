const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('poll', {
    id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    question_text: {
      type: DataTypes.STRING,
      allowNull: true
    },
    panel_id: {
      type: DataTypes.STRING(36),
      allowNull: true,
      references: {
        model: 'panel',
        key: 'id'
      }
    },
    type: {
      type: DataTypes.STRING(30),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'poll',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "poll_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
