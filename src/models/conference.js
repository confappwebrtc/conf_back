const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('conference', {
    id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    title: {
      type: DataTypes.STRING,
      allowNull: true
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true
    },
    image: {
      type: DataTypes.BLOB,
      allowNull: true
    },
    starts: {
      type: DataTypes.DATE,
      allowNull: true
    },
    duration: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    topic: {
      type: DataTypes.STRING,
      allowNull: true
    },
    status: {
      type: DataTypes.STRING(50),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'conference',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "conference_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
