const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('guest', {
    id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    nickname: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    photo: {
      type: DataTypes.BLOB,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'guest',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "guest_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
