const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('chat', {
    id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    date_add: {
      type: DataTypes.DATE,
      allowNull: true
    },
    messege: {
      type: DataTypes.STRING,
      allowNull: true
    },
    panel_id: {
      type: DataTypes.STRING(36),
      allowNull: true,
      references: {
        model: 'panel',
        key: 'id'
      }
    },
    user_id: {
      type: DataTypes.STRING(36),
      allowNull: true,
      references: {
        model: 'users',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'chat',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "chat_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
