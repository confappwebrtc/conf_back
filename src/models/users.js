const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('users', {
    id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    login: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    password: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    speaker_id: {
      type: DataTypes.STRING(36),
      allowNull: true,
      references: {
        model: 'speaker',
        key: 'id'
      }
    },
    promo_id: {
      type: DataTypes.STRING(36),
      allowNull: true,
      references: {
        model: 'promo',
        key: 'id'
      }
    },
    guest_id: {
      type: DataTypes.STRING(36),
      allowNull: true,
      references: {
        model: 'guest',
        key: 'id'
      }
    },
    role_id: {
      type: DataTypes.STRING(36),
      allowNull: true,
      references: {
        model: 'roles',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'users',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "users_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
