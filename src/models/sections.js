const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('sections', {
    id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    title: {
      type: DataTypes.STRING,
      allowNull: true
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true
    },
    conference_id: {
      type: DataTypes.STRING(36),
      allowNull: true,
      references: {
        model: 'conference',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'sections',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "sections_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
