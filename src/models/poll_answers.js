const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('poll_answers', {
    id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    number: {
      type: DataTypes.INTEGER,
      allowNull: false,
      unique: "poll_answers_number_key"
    },
    answer: {
      type: DataTypes.STRING,
      allowNull: true
    },
    question_id: {
      type: DataTypes.STRING(36),
      allowNull: true,
      references: {
        model: 'poll',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'poll_answers',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "poll_answers_number_key",
        unique: true,
        fields: [
          { name: "number" },
        ]
      },
      {
        name: "poll_answers_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
