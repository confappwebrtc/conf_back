const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('role_permission', {
    id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    role_id: {
      type: DataTypes.STRING(36),
      allowNull: true,
      references: {
        model: 'roles',
        key: 'id'
      }
    },
    permissions_id: {
      type: DataTypes.STRING(36),
      allowNull: true,
      references: {
        model: 'permissions',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'role_permission',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "role_permission_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
