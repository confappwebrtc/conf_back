const pg = require('pg')
const dotenv = require('dotenv');
dotenv.config();

//get dates in UNIX fromat
pg.types.setTypeParser(1114, str => new Date(str).getTime()/1000);

const Pool = pg.Pool

var currentPool;
function getPool() {
    pool = currentPool || new Pool({
        user: process.env.PG_USER,
        host: process.env.PG_HOST,
        database: process.env.PG_DATABASE,
        password: process.env.PG_PASSWORD,
        port: process.env.PG_PORT,
      })
    currentPool = pool;
    return pool;
}

module.exports = {
    getPool
}
