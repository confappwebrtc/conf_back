const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
var uuid = require('uuid');
const protobuf = require('../messages/proto.service');
const dotenv = require('dotenv');

dotenv.config();

const db = require('./db.service');

const pool = db.getPool();


/*********************** Permissions ***************************/

const getPermissions = async (request, response) => {
  pool.query('SELECT * FROM public.permissions ORDER BY id ASC', async (error, results) => {
    if (error || results.rows.length === 0) {
      const buf = await protobuf.encode({ error: "No results found" }, "Error");
      response.status(200).send(buf);
    }
    else {
      const buf = await protobuf.encode(results.rows, "Permissions", "permissions");
      response.status(200).send(buf);
    }
  })
}

/*********************** End of Permissions ***************************/




/*********************** Roles ***************************/
const getRoles = async (request, response) => {
  pool.query('SELECT * FROM public.roles;', async (error, results) => {
    if (error || results.rows.length === 0) {
      const buf = await protobuf.encode({ error: "No results found1" }, "Error"); //Rebuilt
      response.status(401).send(buf);
    }
    else {
      const buf = await protobuf.encode(results.rows, "Roles", "roles");
      response.status(200).send(buf);
    }
  })
}

/*********************** End of Roles ***************************/




/*********************** Conferences ***************************/

/* Get All Conferenecs (Conference Contacts Included) */
const getConferences = async (request, response) => {
  pool.query('SELECT public.conference.id, title, description, starts, duration, image, topic, status, email, phone, telegram, instagram, vk FROM public.conference INNER JOIN public.contact_conference ON public.conference.id = public.contact_conference.conference_id;', async (error, results) => {
    if (error || results.rows.length === 0) {

      const buf = await protobuf.encode({ error: "No results found" }, "Error");
      response.status(401).send(buf);

    }
    else {
      const buf = await protobuf.encode(results.rows, "Conferences", "conferences");
      if (buf) response.status(200).send(buf);
      else response.status(401).send();
    }
  })
}



/* Get Conference by its Id (Conference Contacts Included) */
const getConferenceById = async (request, response) => {

  const { id } = request.body.obj;

  pool.query(`SELECT public.conference.id, title, description, image, starts, duration, topic, status, email, phone, telegram, instagram, vk FROM public.conference INNER JOIN public.contact_conference ON public.conference.id = public.contact_conference.conference_id WHERE public.conference.id = '${id}';`, async (error, results) => {
    if (error || results.rows.length === 0) {
      const buf = await protobuf.encode({ error: "No results found" }, "Error");
      response.status(401).send(buf);
    }
    else {
      const buf = await protobuf.encode(results.rows[0], "Conference");
      response.status(200).send(buf);
    }
  })
}


/* Create One Conference */
const createConference = async (request, response) => {
  const { title, description, image, starts, duration, topic, status, email, phone, telegram, instagram, vk } = request.body.obj;

  starts_normalized = new Date(starts * 1000);

  id1 = uuid.v4();
  id2 = uuid.v4();

  pool.query("WITH conference AS (INSERT INTO public.conference (id, title, description, image, starts, duration, topic, status) VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING id), cont as (INSERT INTO public.contact_conference (id, conference_id, email, phone, telegram, instagram, vk) VALUES ($9, $1, $10, $11, $12, $13, $14)) SELECT id FROM conference;",
    [id1, title, description, image, starts_normalized, duration, topic, status, id2, email, phone, telegram, instagram, vk], async (error, results) => {
      if (error) {
        const buf = await protobuf.encode({ error: error }, "Error");
        return response.send(buf);
      }
      response.status(201).send(id1) //Retruns "rows""id"
    })
}


/* Update One Conference */
const updateConference = async (request, response) => {
  const { id, title, description, image, starts, duration, topic, status, email, phone, telegram, instagram, vk } = request.body.obj;

  starts_normalized = new Date(starts * 1000);

  pool.query(
    'WITH conference AS (UPDATE public.conference SET title = $2, description = $3, image = $4, starts = $5, duration = $6, topic = $7, status = $8 WHERE id = $1 RETURNING id), cont as (UPDATE contact_conference SET email = $9, phone = $10, telegram = $10, instagram = $11, vk = $12  WHERE conference_id = $1) SELECT id FROM conference',
    [id, title, description, image, starts_normalized, duration, topic, status, email, phone, telegram, instagram, vk],
    (error, results) => {
      if (error) {
        response.status(404).send()
      }
      response.status(200).send(id)
    }
  )
}

/*********************** End of Conferences ***************************/



/*********************** Sections  ***************************/

/* Get all sections by Conference Id */
const getSections = async (request, response) => {

  const { conference_id } = request.body.obj;

  pool.query(`SELECT * FROM public.sections WHERE conference_id = '${conference_id}';`, async (error, results) => {
    if (error || results.rows.length === 0) {
      const buf = await protobuf.encode({ error: "No results found" }, "Error");
      response.status(401).send(buf);
    }
    else {
      const buf = await protobuf.encode(results.rows, "Sections", "sections");
      response.status(200).send(buf);
    }
  })
}

/* Add one Section */
const addSection = async (request, response) => {

  const { title, description, conference_id } = request.body.obj;

  id = uuid.v4();

  pool.query("INSERT INTO public.sections( id, title, description, conference_id) VALUES ($1, $2, $3, $4);",
    [id, title, description, conference_id], async (error, results) => {
      if (error) {
        const buf = await protobuf.encode({ error: error }, "Error");
        response.status(401).send(buf);
      }
      response.status(201).send(id)
    })
}

/* Update One Section */
const updateSection = async (request, response) => {

  const { id, title, description, conference_id } = request.body.obj;

  pool.query(
    'UPDATE public.sections SET title = $2, description = $3, conference_id = $4 WHERE id = $1',
    [id, title, description, conference_id],
    async (error, results) => {
      if (error) {
        const buf = await protobuf.encode({ error: error }, "Error");
        response.status(401).send(buf);
      }
      response.status(201).send(id)
    }
  )
}

/*********************** End of Sections  ***************************/


/*********************** Panels  ***************************/

/* Get all Panels by Section Id */
const getPanelsBySection = async (request, response) => {

  const { id } = request.body.obj;

  pool.query(`SELECT * FROM public.panel WHERE section_id = '${id}';`, async (error, results) => {
    if (error || results.rows.length === 0) {
      const buf = await protobuf.encode({ error: "No results found" }, "Error");
      response.status(401).send(buf);
    }
    else {
      const buf = await protobuf.encode(results.rows, "Panels", "panels");
      response.status(200).send(buf);
    }
  })
}

/* Get all Panels by Section Id with User */
/*
const getPanelsBySectionWithUsers = async (request, response) => {

}
*/

/* Get One Panel by its Id */
const getPanelById = async (request, response) => {

  const { id } = request.body.obj;

  pool.query(`SELECT * FROM public.panel WHERE id = '${id}';`, async (error, results) => {
    if (error || results.rows.length === 0) {
      const buf = await protobuf.encode({ error: "No results found" }, "Error");
      response.status(401).send(buf);
    }
    else {
      const buf = await protobuf.encode(results.rows[0], "Panel");
      response.status(200).send(buf);
    }
  })
}


/* Add One Panel */
const addPanel = async (request, response) => {

  const { title, description, starts, duration, section_id, user_id, rating, rating_count, status } = request.body.obj;

  starts_normalized = new Date(starts * 1000);

  id = uuid.v4();
  pool.query("INSERT INTO public.panel( id, title, description, starts, duration, section_id, user_id, rating, rating_count, status) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9);",
    [id, title, description, starts_normalized, duration, section_id, user_id, rating, status], (error, results) => {
      if (error) {
        throw error
      }
      response.status(201).send(id) //Retruns "rows""id"
    })
}

/* Update One Panel */
const updatePanel = async (request, response) => {

  const { id, title, description, starts_UNIX, duration, section_id, user_id, rating, rating_count, status } = request.body.obj;
  starts = new Date(starts_UNIX * 1000);

  pool.query(
    'UPDATE public.sections SET title = $2, description = $3, starts = $4, duration = $5, section_id = $6, user_id = $7, rating = $8, rating_count = $9, status = $10 WHERE id = $1',
    [id, title, description, starts, duration, section_id, user_id, rating, rating_count, status],
    async (error, results) => {
      if (error) {
        const buf = await protobuf.encode({ error: error }, "Error");
        return response.status(401).send(buf);
      }
      response.status(201).send(id)
    }
  )
}
/*********************** End of Panels  ***************************/




/*********************** start of Polls  ***************************/
const getPollsByPanel = async (request, response) => {

  const obj = request.body.obj;
  const { id } = obj;
  const client = await pool.connect()
  try {
    const queryText = `SELECT id, question_text, panel_id FROM public.poll WHERE panel_id = '${id}';`
    const res = await client.query(queryText)
    polls = res.rows;
    if (polls == null || !polls.length) response.status(401).json({ error: "Poll does not exist" });
    for(i=0; i< polls.length; i++){
      polls[i]["question"] = polls[i].question_text
      if (polls[i].id != null) {
        const secondQuery = `SELECT * FROM public.poll_answers WHERE question_id = '${polls[i].id}';`
        res2 = await client.query(secondQuery);
        if (res2.rows != null && res2.rows.length) polls[i]["answers"] = res2.rows
      }
    }
        
        console.log(polls);
        user_buf = await protobuf.encode(polls, "Polls", "polls");
        return response.status(201).send(user_buf);
  }catch(e) {
    console.log(e);
    response.status(401).json({ error: "Poll does not exist" });
  }
}



/*********************** Users  ***************************/

/* Add User (login, password, Role) */
const registerUserWithRole = async (request, response) => {

  const { login, password, role } = request.body.obj;
  if (!role || !role.id) {
    const buf = await protobuf.encode({ error: "Please Add a Valid Role" }, "Error");
    return response.status(409).send(buf);
  }
  role_id = role.id;

  pool.query(`SELECT count(*) FROM public.users where public.users.login = '${login}';`,
    async (error, results) => {
      if (results.rows[0].count === 1) {
        const buf = await protobuf.encode({ error: error }, "User Already Exist. Please Login");
        return response.status(401).send(buf);
      }
    })

  // generate salt to hash password
  const salt = await bcrypt.genSalt(10);
  // now we set user password to hashed password
  password_hashed = await bcrypt.hash(password, salt);
  id = uuid.v4();


  pool.query("INSERT INTO public.users(id, login, password, role_id) VALUES ($1, $2, $3, $4);",
    [id, login, password_hashed, role_id], async (error, results) => {
      if (error) {
        const buf = await protobuf.encode({ error: "Please Sign Up or Login" }, "Error");
        return response.status(409).send(buf);
      }
      response.status(201).send(id) //Retruns "rows""id"
    })
}



/* Add User (login, password, guest) */
const registerUser = async (request, response) => {

  if ('guest' in request.body.obj) {
    const { login, password, guest } = request.body.obj;

    pool.query(`SELECT count(*) FROM public.users where public.users.login = '${login}';`,
      async (error, results) => {
        if (results.rows[0].count === 1) {
          const buf = await protobuf.encode({ error: error }, "User Already Exist. Please Login");
          return response.status(401).send(buf);
        }
      })

    // generate salt to hash password
    const salt = await bcrypt.genSalt(10);
    // now we set user password to hashed password
    password_hashed = await bcrypt.hash(password, salt);

    id1 = uuid.v4();
    id2 = uuid.v4();
    const { nickname, photo } = guest;
    pool.query("WITH userRows AS (INSERT INTO public.users (id, login, password, guest_id) VALUES ($1, $2, $3, $4) RETURNING id), guest as (INSERT INTO public.guest (id, nickname, photo) VALUES ($4, $5, $6)) SELECT id FROM userRows;",
      [id1, login, password_hashed, id2, nickname, photo],
      async (error, results) => {
        if (error || results.rows.length === 0) {
          const buf = await protobuf.encode({ error: "User Cannot Be Added 2"}, "Error");
          return response.status(409).send(buf);
        }
        else return response.status(201).send(id1) //could return user here
      })
  }
  else {
    const buf = await protobuf.encode({ error: "User Cannnot Be Added" }, "Error");
    return response.status(409).send(buf);
  }
}

/* Update User (login, password only) */
const updateUser = async (request, response) => {

  const { id, login, password } = request.body.obj;
  // generate salt to hash password
  const salt = await bcrypt.genSalt(10);
  // now we set user password to hashed password
  password_hashed = await bcrypt.hash(password, salt);


  pool.query('UPDATE public.users SET login = $2, password = $3 WHERE id = $1',
    [id, login, password_hashed], async (error, results) => {
      if (error) {
        const buf = await protobuf.encode({ error: "User Wasn't Upadated" }, "Error");
        return response.status(401).send(buf);
      }
      response.status(201).send(id)
    })
}




/* Create Complete User With Type Defined */
const registerUserMod = async (request, response) => {
  const obj = request.body.obj;

  pool.query(`SELECT count(*) FROM public.users where public.users.login = '${obj.login}';`,
    async (error, results) => {
      if (results.rows[0].count === 1) {
        const buf = await protobuf.encode({ error: error }, "User Already Exist. Please Login");
        return response.status(401).send(buf);
      }
    })

  /* Check request for Promo || Speaker || Guest */
  /* 1. User with Promo */
  if ('promo' in obj) {
    const { login, password, promo } = obj;
    id1 = uuid.v4();
    id2 = uuid.v4();
    const { title, about, nickname, photo, address, invited, status } = promo;
    pool.query("WITH user AS (INSERT INTO public.users (id, login, password, promo_id) VALUES ($1, $2, $3, $4) RETURNING id), promo as (INSERT INTO public.promo (id, title, about, nickname, photo, address, invited, status) VALUES ($4, $5, $6, $7, $8, $9, $10)) SELECT id FROM user;",
      [id1, login, password, id2, title, about, nickname, photo, address, invited, status],
      async (error, results) => {
        if (error || results.rows.length === 0) {
          const buf = await protobuf.encode({ error: "User Cannnot Be Added" }, "Error");
          response.status(401).send(buf);
        }
        response.status(201).send(id1) //could return user here
      })
  }
  /* End of 1. User with Promo */

  /* 2. User with Speaker */
  else if ('speaker' in obj) {
    const { login, password, speaker } = obj;
    id1 = uuid.v4();
    id2 = uuid.v4();
    const { lastname, firstname, middlename, nickname, photo, city, gender, dob, about, job, company, invited, status } = speaker;
    pool.query("WITH user AS (INSERT INTO public.users (id, login, password, speaker_id) VALUES ($1, $2, $3, $4) RETURNING id), speaker as (INSERT INTO public.speaker (id, title, about, nickname, photo, address, invited, status) VALUES ($4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17)) SELECT id FROM user;",
      [id1, login, password, id2, lastname, firstname, middlename, nickname, photo, city, gender, dob, about, job, company, invited, status],
      async (error, results) => {
        if (error || results.rows.length === 0) {
          const buf = await protobuf.encode({ error: "User Cannnot Be Added" }, "Error");
          response.status(401).send(buf);
        }
        response.status(201).send(id1) //could return user here
      })
  }
  /* End of 2. User with Speaker */

  /* 3. User with Guest */
  else if ('guest' in obj) {
    const { login, password, guest } = obj;
    id1 = uuid.v4();
    id2 = uuid.v4();
    const { nickname, photo } = guest;
    pool.query("WITH user AS (INSERT INTO public.users (id, login, password, guest_id) VALUES ($1, $2, $3, $4) RETURNING id), guest as (INSERT INTO public.guest (id, nickname, photo) VALUES ($4, $5, $6)) SELECT id FROM user;",
      [id1, login, password, id2, nickname, photo],
      async (error, results) => {
        if (error || results.rows.length === 0) {
          const buf = await protobuf.encode({ error: "User Cannnot Be Added" }, "Error");
          response.status(401).send(buf);
        }
        response.status(201).send(id1) //could return user here
      })
  }
  /* End of 3. User with Guest */
  else {
    const buf = await protobuf.encode({ error: "User Type not Found" }, "Error");
    response.status(401).send(buf);
  }
}


/* Update Complete User With Type Defined */
const updateUserInfo = async (request, response) => {
  const obj = request.body.obj;

  const { id } = obj;
  const client = await pool.connect()
  try {
    const queryText = `SELECT id, login, promo_id, speaker_id, guest_id FROM public.users WHERE id = '${id}';`
    const res = await client.query(queryText)
    user = res.rows[0];
    if (user == null) return response.status(401).json({ error: "User does not exist" });

    if (user.promo_id != null && 'promo' in obj) {
      const { promo } = obj;
      const secondQuery = `UPDATE public.promo SET title = '${promo.title}', about = '${promo.about}', nickname = '${promo.nickname}', photo = '${promo.photo}', address = '${promo.address}', invited = '${promo.invited}', status = '${promo.status}' WHERE id = '${user.promo_id}';`
      res2 = await client.query(secondQuery);
      if (res2.rows[0] != null) {
        return response.status(201).send(id);
      }
    }

    if (user.speaker_id != null && 'speaker' in obj) {
      const { speaker } = obj;
      const secondQuery = `UPDATE public.speaker SET lastname = '${speaker.lastname}', firstname = '${speaker.firstname}', middlename = '${speaker.middlename}', nickname = '${speaker.nickname}', city = '${speaker.city}', gender = '${speaker.gender}', dob = '${speaker.dob}', about = '${speaker.about}', job = '${speaker.job}', company = '${speaker.company}', invited = '${speaker.invited}', status = '${speaker.status}' WHERE id = '${user.speaker_id}';`
      res2 = await client.query(secondQuery);
      if (res2.rows[0] != null) {
        return response.status(201).send(id);
      }
    }

    if (user.guest_id != null && 'guest' in obj) {
      const { guest } = obj;
      const secondQuery = `UPDATE public.guest SET nickname = '${guest.nickname}', photo = '${guest.photo}' WHERE id = '${user.speaker_id}';`
      res2 = await client.query(secondQuery);
      if (res2.rows[0] != null) {
        return response.status(201).send(id);
      }
    }

  } catch (e) {
    const buf = await protobuf.encode({ error: "User Info Couldn't be Updated" }, "Error");
    response.status(409).send(buf);
  }
}



/* Get User Complete With Id */
const getUser = async (request, response) => {
  const obj = request.body.obj;
  const { id } = obj;
  const client = await pool.connect()
  try {
    const queryText = `SELECT id, login, promo_id, speaker_id, guest_id FROM public.users WHERE id = '${id}';`
    const res = await client.query(queryText)
    user = res.rows[0];
    if (user == null) response.status(401).json({ error: "User does not exist" });

    if (user.promo_id != null) {
      const secondQuery = `SELECT * FROM public.promo WHERE id = '${user.promo_id}';`
      res2 = await client.query(secondQuery);
      if (res2.rows[0] != null) {
        user_buf = await protobuf.encode({ "id": id, "login": user.login, "promo": res2.rows[0] }, "User");
        return response.status(201).send(user_buf);
      }
    }
    if (user.speaker_id != null) {
      const secondQuery = `SELECT * FROM public.speaker WHERE id = '${user.speaker_id}';`
      res2 = await client.query(secondQuery);
      if (res2.rows[0] != null) {
        user_buf = await protobuf.encode({ "id": id, "login": user.login, "speaker": res2.rows[0] }, "User");
        return response.status(201).send(user_buf);
      }
    }
    if (user.guest_id != null) {
      const secondQuery = `SELECT * FROM public.guest WHERE id = '${user.guest_id}';`
      res2 = await client.query(secondQuery);
      if (res2.rows[0] != null) {
        user_buf = await protobuf.encode({ "id": id, "login": user.login, "guest": res2.rows[0] }, "User");
        return response.status(201).send(user_buf);
      }
    }
  } catch (e) {
    const buf = await protobuf.encode({ error: "Wrong Object Type" }, "Error");
    return response.status(401).send(buf);
  }
}




/* User Login Check */
const loginUser = async (request, response) => {
  const obj = request.body.obj;
  const { login, password } = obj;
  pool.query(`SELECT * FROM public.users WHERE login = '${login}';`, async (error, results) => {
    if (error) {
      const buf = await protobuf.encode({ error: "User not Found" }, "Error");
      return response.status(401).send(buf);
    }
    if (results.rows[0]) {
      const validPassword = await bcrypt.compare(password, results.rows[0].password);
      if (validPassword) {
        // Create token
        delete results.rows[0].password;

        const token = jwt.sign(results.rows[0], process.env.TOKEN_KEY, { expiresIn: "2h", });
        return response.status(200).send({ token: token }); //return user id here
      }
      else return response.status(400).json({ error: "Invalid Password" });
    }
    else response.status(401).json({ error: "User does not exist" });
  })
}




/*
const addSpeaker = async (request, response) => {
  const root = await protobuf.load('user.proto');
  const Speaker = root.lookupType('conferencepackage.Speaker');
  const obj = Speaker.decode(Buffer.from(request.body));
  const { lastname, firstname, middlename, nickname, photo, city, gender, dob, about, job, company, invited, status } = obj;

  id = uuid.v4();

  pool.query("INSERT INTO public.speaker(id, lastname, firstname, middlename, nickname, photo, city, gender, dob, about, job, company, invited, status) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14);",
    [id, lastname, firstname, middlename, nickname, photo, city, gender, dob, about, job, company, invited, status], (error, results) => {
      if (error) throw error;
      response.status(201).send(id) //Retruns "rows""id"
    })
}

const updateSpeaker = async (request, response) => {
  const root = await protobuf.load('user.proto');
  const Speaker = root.lookupType('conferencepackage.Speaker');
  const obj = Speaker.decode(Buffer.from(request.body));
  const { id, lastname, firstname, middlename, nickname, photo, city, gender, dob, about, job, company, invited, status } = obj;

  if(id != null)
    pool.query("UPDATE public.speaker SET lastname = $2, firstname = $3, middlename = $4, nickname = $5, photo = $6, city = $7, gender = $8, dob = $9, about = $10, job= $11, company = $12, invited = $13, status = $14 WHERE id = $1;",
      [id, lastname, firstname, middlename, nickname, photo, city, gender, dob, about, job, company, invited, status], (error, results) => {
        if (error) throw error;
        response.status(201).send(id) //Retruns "rows""id"
      })
}

const addPromo = async (request, response) => {
  const root = await protobuf.load('user.proto');
  const Promo = root.lookupType('conferencepackage.Promo');
  const obj = Promo.decode(Buffer.from(request.body));
  const { title, about, nickname, photo, address, invited, status } = obj;

  id = uuid.v4();

  pool.query("INSERT INTO public.promo(id, title, about, nickname, photo, address, invited, status) VALUES ($1, $2, $3, $4, $5, $6, $7, $8);",
    [id, title, about, nickname, photo, address, invited, status], (error, results) => {
      if (error) throw error;
      response.status(201).send(id) //Retruns "rows""id"
    })
}

const updatePromo = async (request, response) => {
  const root = await protobuf.load('user.proto');
  const Promo = root.lookupType('conferencepackage.Promo');
  const obj = Promo.decode(Buffer.from(request.body));
  const {id, title, about, nickname, photo, address, invited, status } = obj;

  if(id != null)
    pool.query("UPDATE public.promo SET title = $2, about = $3, nickname = $4, photo = $5, address = $6, invited = $7, status = $8 WHERE id = $1;",
      [id, title, about, nickname, photo, address, invited, status], (error, results) => {
        if (error) throw error;
        response.status(201).send(id) //Retruns "rows""id"
      })
}

*/





module.exports = {
  getPermissions,
  getRoles,
  getConferences,
  getConferenceById,
  createConference,
  updateConference,
  getSections,
  addSection,
  updateSection,
  addPanel,
  getPanelsBySection,
  getPanelById,
  updatePanel,
  getPollsByPanel,
  registerUser,
  registerUserWithRole,
  updateUser,
  registerUserMod,
  updateUserInfo,
  getUser,
  loginUser
}

/*

  addSpeaker,
  updateSpeaker,
  addPromo,
  updatePromo

*/
